package com.mastertech.game.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mastertech.game.web.model.StartGame;

@RestController
public class GameFake {

	@RequestMapping(path="/game/start")
	public StartGame gameFake() {
		StartGame startGame = new StartGame();
		startGame.setId(1);
		startGame.setNomeUsuario("TesteEdu");
		startGame.setQtdeAcertos(10);
		startGame.setQtdePerguntas(2);
		startGame.setListaPerguntas(null);
		return startGame;
	}	
}
