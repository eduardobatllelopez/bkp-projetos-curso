package com.mastertech.game.web.controller;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;

public class SimpleLogEngine extends LogEngine{

	@Override
	public void log(String message) {
		System.out.println(message);
		ProducerRankingKafka producerRankingKafka = new ProducerRankingKafka();
		producerRankingKafka.GeraLog(message);
	}
	
	
	
}
