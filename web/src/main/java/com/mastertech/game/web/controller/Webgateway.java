package com.mastertech.game.web.controller;

import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.mastertech.game.web.model.ListaPerguntas;
import com.mastertech.game.web.model.Perguntas;
import com.mastertech.game.web.model.Ranking;
import com.mastertech.game.web.model.StartGame;

@RestController
public class Webgateway {
	
//	Get start game
	private RestTemplate restTemplate = new RestTemplate();
	@RequestMapping(path="/game/start/{nome}")
	public StartGame getStartGame(@PathVariable String nome) {
		String url = "http://10.162.109.70:8080/game/start/"+ nome;
		System.out.println("Url--->" + url);
		System.out.println("StartGame---> " + StartGame.class);
		System.out.println("RestTemplate---> " + RestTemplate.class);
		return restTemplate.getForObject(url, StartGame.class);
	}

//	Post resposta das perguntas
	private RestTemplate restTemplate1 = new RestTemplate();
	@RequestMapping(method=RequestMethod.POST, path="/game/start/respostas/{id}")
	public ListaPerguntas respostas(@PathVariable int id, @RequestBody List<Perguntas> novoPerguntas) {
		String url = "http://10.162.109.70:8080/game/start/perguntas/" + id;
		System.out.println("Url--->" + url);
		System.out.println("Perguntas--> " + novoPerguntas);
		return restTemplate1.postForObject(url, novoPerguntas, ListaPerguntas.class);
	}
	
//	Get perguntas
	private RestTemplate restTemplate2 = new RestTemplate();
	@RequestMapping(path="/game/start/perguntas/{id}")
	public StartGame getPerguntas(@PathVariable Integer id) {
		String url = "http://10.162.109.70:8080/game/start/perguntas/"+ id;
		System.out.println("Url--->" + url);
		System.out.println("StartGame---> " + StartGame.class);
		System.out.println("RestTemplate---> " + RestTemplate.class);
		return restTemplate2.getForObject(url, StartGame.class);
	}
	
//	Post ranking
	private RestTemplate restTemplate3 = new RestTemplate();
	@RequestMapping(method=RequestMethod.POST, path="/ranking")
	public Ranking ranking(@RequestBody Ranking novoRanking) {		
		String url = "http://10.162.106.242:8080/ranking";
		System.out.println("Url--->" + url);
		System.out.println("Ranking---> " + novoRanking);		
		return restTemplate3.postForObject(url, new HttpEntity<Ranking>(novoRanking), Ranking.class);
	}

//	Get ranking nome jogo
	private RestTemplate restTemplate4 = new RestTemplate();
	@RequestMapping(path="/rankingJogo/{nomeJogo}")
	public ListaPerguntas getListaPerguntas(@PathVariable String nomeJogo) {
		String url = "http://10.162.106.242:8080/rankingJogo/"+ nomeJogo;
		System.out.println("Url--->" + url);
		System.out.println("Ranking---> " + ListaPerguntas.class);
		System.out.println("RestTemplate---> " + RestTemplate.class);
		return restTemplate4.getForObject(url, ListaPerguntas.class);
	}
	
//	Get ranking nome jogador
	private RestTemplate restTemplate5 = new RestTemplate();
	@RequestMapping(path="/rankingJogador/{nomeJogador}")
	public Ranking getRanking2(@PathVariable String nomeJogador) {
		String url = "http://10.162.106.242:8080/rankingJogador/"+ nomeJogador;
		System.out.println("Url--->" + url);
		System.out.println("Ranking---> " + Ranking.class);
		System.out.println("RestTemplate---> " + RestTemplate.class);
		return restTemplate.getForObject(url, Ranking.class);
	}
	
//	Post ranking
	ProducerRankingKafka producerRankingKafka = new ProducerRankingKafka();
	
	@RequestMapping(method=RequestMethod.POST, path="/rankingKafka")
	public Ranking ranking1(@RequestBody Ranking novoRanking) {		
		System.out.println("Ranking---> " + novoRanking);
		System.out.println("nomeJogador---> " + novoRanking.nomeJogador);
		TrackerApp.Xpto2(novoRanking);
//		producerRankingKafka.GeraLog(novoRanking.nomeJogador);
		System.out.println("Depois do producer--->");
		return novoRanking;
	}
	
}