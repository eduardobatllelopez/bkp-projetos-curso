package com.mastertech.game.web.model;

import java.util.ArrayList;
import java.util.List;

public class ListaPerguntas {
	
	public List<Perguntas> listaDePerguntas;

	public List<Perguntas> getListaDePerguntas() {
		return listaDePerguntas;
	}

	public void setListaDePerguntas(List<Perguntas> listaDePerguntas) {
		this.listaDePerguntas = listaDePerguntas;
	}
	
}
