package com.mastertech.game.web.controller;

public abstract class LogEngineFactory {

	public static final int SIMPLE = 0;

	public static LogEngine getEngine(int type) {
		switch (type) {
		case SIMPLE:
			return new SimpleLogEngine();
		default:
			return null;
		}
	}
}
