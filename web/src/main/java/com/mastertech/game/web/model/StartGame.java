package com.mastertech.game.web.model;

import java.util.ArrayList;

public class StartGame {

	int id;
	String nomeUsuario;
	int qtdePerguntas;
	int qtdeAcertos;
	ArrayList<Perguntas> listaPerguntas;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNomeUsuario() {
		return nomeUsuario;
	}
	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}
	public int getQtdePerguntas() {
		return qtdePerguntas;
	}
	public void setQtdePerguntas(int qtdePerguntas) {
		this.qtdePerguntas = qtdePerguntas;
	}
	public int getQtdeAcertos() {
		return qtdeAcertos;
	}
	public void setQtdeAcertos(int qtdeAcertos) {
		this.qtdeAcertos = qtdeAcertos;
	}
	public ArrayList<Perguntas> getListaPerguntas() {
		return listaPerguntas;
	}
	public void setListaPerguntas(ArrayList<Perguntas> listaPerguntas) {
		this.listaPerguntas = listaPerguntas;
	}
	
}
