package com.mastertech.game.web;

import java.util.Random;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

public class ProducerRanking {

	public static void xpto() {
		try {
			// Abrir conexão com o broker
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://amq.oramosmarcos.com:61616");
        	Connection connection = connectionFactory.createConnection();
        	connection.start();
        	
        	// Criar uma sessão
        	Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        	
        	// A fila que queremos popular
        	Queue queue = session.createQueue("a.queue.ranking"); 
        	
        	// Nosso produtor de eventos
        	MessageProducer producer = session.createProducer(queue);
        	
        	long n = 0;
        	Random random = new Random();
        	while(true) {
        		MapMessage msg = session.createMapMessage();
        		n++;
        		msg.setString("playerName", "Player " + Long.toString( n % 5));
        		msg.setString("gameId", "game-" + Integer.toString(random.nextInt(4)));
        		msg.setInt("hits", random.nextInt(80));
        		msg.setInt("misses", 80 + random.nextInt(20));
        		msg.setString("total", Long.toString( n % 5));
        		producer.send(msg);
        		Thread.sleep(100);
        		System.out.println("Sent: " + msg.getJMSMessageID());
        	}
		
		} catch(JMSException | InterruptedException ex) {
			ex.printStackTrace();
		}        
	}

}
