package com.mastertech.game.web.controller;

public class Events {
	
	public static void emmit(String source, String action) {
		emmit(source, action, null);
	}
	
	public static void emmit(String source, String action, Trackable trackable) {
		Event event = new Event(source, action, trackable);
		LogEngine logger = LogEngineFactory.getEngine(LogEngineFactory.SIMPLE);
		logger.log(event);
	}
}
