package com.mastertech.game.web.model;

import java.util.HashMap;
import java.util.Map;

import com.mastertech.game.web.controller.Trackable;

public class Ranking implements Trackable{

	public String nomeJogo;
	public String nomeJogador;
	public Integer pontuacao;
	
	public Ranking(String nomeJogo, String nomeJogador, Integer pontuacao) {
		super();
		this.nomeJogo = nomeJogo;
		this.nomeJogador = nomeJogador;
		this.pontuacao = pontuacao;
	}
	
	public String getNomeJogo() {
		return nomeJogo;
	}
	public void setNomeJogo(String nomeJogo) {
		this.nomeJogo = nomeJogo;
	}
	public String getNomeJogador() {
		return nomeJogador;
	}
	public void setNomeJogador(String nomeJogador) {
		this.nomeJogador = nomeJogador;
	}
	public Integer getPontuacao() {
		return pontuacao;
	}
	public void setPontuacao(Integer pontuacao) {
		this.pontuacao = pontuacao;
	}
	
	// Implementação das funções de Trackable

	public String getTrackedType() {
		return Ranking.class.getSimpleName();
	}
	
	public Map<String, String> getTrackedProperties() {
		Map<String, String>  map = new HashMap<String, String>();
		map.put("nomeJogo", String.valueOf(nomeJogo));
		map.put("nomeJogador", String.valueOf(nomeJogador));
		return map;
	}

}
