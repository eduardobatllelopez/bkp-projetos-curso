package com.mastertech.game.web.controller;

import java.util.Random;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;

public class RPCClient {

	private static String QUEUE_NAME = "rpc.queue";
	private static String AMQ_SERVER = "tcp://amq.oramosmarcos.com:61616";

	public static void Xpto(String[] args) {
		try {
			// Abrir conexão com o broker
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(AMQ_SERVER);
			Connection connection = connectionFactory.createConnection();
			connection.start();

			// Criar uma sessão
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// A fila que queremos popular
			Queue queue = session.createQueue(QUEUE_NAME);

			// Nosso produtor de eventos
			MessageProducer producer = session.createProducer(queue);

			Random random = new Random();
			while (true) {
				
				// Criar uma fila temporária
				Destination tempQueue = session.createTemporaryQueue();
				
				// Criar o consumidor da fila temporária
				MessageConsumer responseConsumer = session.createConsumer(tempQueue);
				
				// Construir a mensagem
				int n = random.nextInt(100);
				MapMessage msg = session.createMapMessage();
				msg.setInt("value", n);
				
				// Configurar os parâmetros de resposta
				msg.setJMSCorrelationID("val-req-" + n);
				msg.setJMSReplyTo(tempQueue);
				
				// Envia a mensagem
				producer.send(msg);
				System.out.println("Sent: " + msg.getJMSMessageID());
				
				// Get the response
				MapMessage response = (MapMessage) responseConsumer.receive(1000);
				
				System.out.println("--------");
				System.out.println("value:  " + response.getInt("value"));
				System.out.println("factor: " + response.getInt("factor"));
				System.out.println("result: " + response.getInt("result"));
				System.out.println("--------");
				
				responseConsumer.close();
				
				Thread.sleep(100);
				
			}

		} catch (JMSException | InterruptedException ex) {
			ex.printStackTrace();
		}
	}

}