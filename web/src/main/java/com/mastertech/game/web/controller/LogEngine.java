package com.mastertech.game.web.controller;

public abstract class LogEngine {

	public void log(Event message) {
		log(message.toString());
	}
	
	public abstract void log(String message);
}
