package com.mastertech.game.web.controller;

import java.util.Map;

/**
 * Contrato de todas as classes que são "rastreáveis"
 *
 */
public interface Trackable {

	/**
	 * Retorna o tipo desse objeto (player, game, 
	 * @return
	 */
	public String getTrackedType();
	
	/**
	 * 
	 * @return
	 */
	public Map<String, String> getTrackedProperties();
	
}
