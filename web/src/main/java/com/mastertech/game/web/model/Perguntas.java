package com.mastertech.game.web.model;

public class Perguntas {

	public int id;
	public String titulo;
	public String categoria;
	public String opcao1;
	public String opcao2;
	public String opcao3;
	public String opcao4;
	public String resposta;
	public String respostaUsuario;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getOpcao1() {
		return opcao1;
	}
	public void setOpcao1(String opcao1) {
		this.opcao1 = opcao1;
	}
	public String getOpcao2() {
		return opcao2;
	}
	public void setOpcao2(String opcao2) {
		this.opcao2 = opcao2;
	}
	public String getOpcao3() {
		return opcao3;
	}
	public void setOpcao3(String opcao3) {
		this.opcao3 = opcao3;
	}
	public String getOpcao4() {
		return opcao4;
	}
	public void setOpcao4(String opcao4) {
		this.opcao4 = opcao4;
	}
	public String getResposta() {
		return resposta;
	}
	public void setResposta(String resposta) {
		this.resposta = resposta;
	}
	public String getRespostaUsuario() {
		return respostaUsuario;
	}
	public void setRespostaUsuario(String respostaUsuario) {
		this.respostaUsuario = respostaUsuario;
	}

}

