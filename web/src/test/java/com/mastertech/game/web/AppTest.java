package com.mastertech.game.web;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

import com.mastertech.game.web.controller.Webgateway;
import com.mastertech.game.web.model.StartGame;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
@SpringBootApplication
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
                
    }
    
//    public void testaWebgateway() {
//    	StartGame startGame = new StartGame();
//    	System.out.println("Teste----->" + startGame);
//    	assertEquals(startGame, "com.mastertech.game.web.model.StartGame@15327b79");
//    	System.out.println("Teste--depois--->" + startGame);
//    }  
    
    @org.junit.Test
	public void testeWebgateway(){
		String url = String.format("http://localhost:8080/game/start/eduabc");

    	Webgateway webGateway = new Webgateway();
    	StartGame startGame = new StartGame();
    	
    	StartGame ret = webGateway.getStartGame(url);
    	
		System.out.print("Retorno webGateway---->" + webGateway);		
		System.out.print("Retorno startGame----->" + startGame + "retorno--->" + ret);

//		assertEquals(0, placar.pontuacao[0]);
//		assertEquals(0, placar.pontuacao[1]);
	}
    
}
