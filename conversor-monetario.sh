#!/bin/bash

MOEDA_ORIGEM=$1
MOEDA_DESTINO=$2
VALOR=$3
URL="https://api.exchangeratesapi.io/latest?base=$MOEDA_ORIGEM&symbols=$MOEDA_DESTINO"

COTACAO=`curl $URL | jq ".rates.$MOEDA_DESTINO"`

VALOR_CONVERTIDO=`python -c 'print ('$VALOR' * '$COTACAO')'` 

echo $VALOR_CONVERTIDO

